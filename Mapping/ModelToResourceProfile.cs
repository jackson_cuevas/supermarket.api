using AutoMapper;
using Supermarket.API.Resources;
using Supermarket.API.Domain.Models;
using Supermarket.API.Extensions;

namespace Supermarket.API.Mapping
{
    public class ModelToResourceProfile : Profile
    {
        public ModelToResourceProfile()
        {
            CreateMap<Category, CategoryResource>();

            CreateMap<Product, ProductResource>() 
                .ForMember(src => src.UnitOfMesurement,
                            opt => opt.MapFrom(src => src.UnitOfMeasurement.ToDescriptionString()));
        }
    }
}