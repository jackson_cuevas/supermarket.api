using Supermarket.API.Resources;

namespace Supermarket.API.Resources
{
    public class ProductResource
    {
        public int Id { get; set; } 
        public string Name { get; set; }
        public int QuantityInPackge { get; set; }
        public string UnitOfMesurement { get; set; }
        public CategoryResource Category { get; set; }
    }
}